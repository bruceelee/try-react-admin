import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import {fetchAuth, removeSession} from '../../actions';
import { authServer } from '../../api';

import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';


class FirstBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  handleLoginAuth = (event) =>{
      const email = 'campos.esteban@gmail.com';
      const { fetchAuth } = this.props;
      fetchAuth(email);
      /*authServer().then(response=>{
        console.log(response);
      })*/

  };

 handleCloseSession = (event) =>{
     this.handleRequestClose();
     const { removeSession } = this.props;
     removeSession();
 }

 goDashboard = (event)=>{
   console.log(this.props);
   this.props.router.push(`/app/dashboard`);
 }

  render(){
    const { isConnected } = this.props;

    return(
      <header>
        <div className="header-container">
          <div className="header-top">
            <div className="container">
              <div className="row">
                {/*<div className="col-lg-4 col-sm-4 hidden-xs">
                  <div className="welcome-msg " >Welcome to MyStore online shopping </div>
                </div> */}

                <div className="headerlinkmenu col-md-12">
                  <div className="links">
                    <div className="myaccount">
                    {
                        isConnected ?

                        <a title="My Account" onTouchTap={this.handleTouchTap}>
                            <span>Mi cuenta</span>
                            <i className="fa fa-user-circle fa-2x" style={{'color':'#AF3789'}}></i>
                        </a>

                        :

                        <FlatButton
                          onClick={this.handleLoginAuth}
                          target="_blank"
                          label="Login con Facebook"
                          secondary={true}
                          icon={<FontIcon className="muidocs-icon-custom-github" />}
                        />

                    }
                    </div>
                       <Popover
                          open={this.state.open}
                          anchorEl={this.state.anchorEl}
                          anchorOrigin= {{"horizontal":"right","vertical":"top"}}
                          targetOrigin= {{"horizontal":"right","vertical":"top"}}
                          onRequestClose={this.handleRequestClose}
                        >
                          <Menu>
                            <MenuItem primaryText="Ver Perfil" />
                            <MenuItem primaryText="Mis donaciones" />
                            <MenuItem primaryText="Mis postulaciones" />
                            <MenuItem primaryText="Cerrar Sesión" onClick={this.handleCloseSession}/>
                          </Menu>
                        </Popover>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-sm-4 col-md-3 col-xs-12">
                <div className="logo"><a title="e-commerce" onClick={this.goDashboard}><img alt="e-commerce" src="assets/images/logo.png"/></a> </div>
              </div>
              <div className="col-md-9 col-sm-8 hidden-xs">
                <div className="mtmegamenu">
                  <ul>
                    <li className="mt-root demo_custom_link_cms">
                      <div className="mt-root-item"><a>
                          <div className="title title_font"><span className="title-text">Inicio</span></div>
                        </a></div>
                    </li>
                    <li className="mt-root">
                      <div className="mt-root-item"><a>
                        <div className="title title_font"><span className="title-text">Quienes Somos</span></div>
                        </a></div>
                    </li>
                    <li className="mt-root">
                      <div className="mt-root-item"><a>
                        <div className="title title_font"><span className="title-text">Noticias y Testimonios</span> </div>
                        </a></div>
                    </li>
                    <li className="mt-root">
                      <div className="mt-root-item"><a>
                        <div className="title title_font"><span className="title-text">Donar</span></div>
                        </a></div>
                    </li>
                    <li className="mt-root demo_custom_link_cms">
                      <div className="mt-root-item"><a>
                        <div className="title title_font"><span className="title-text">Contacto</span></div>
                        </a></div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    )
  }
}


const mapStateToProps = state => ({
  isConnected: state.auth && state.auth.token ? state.auth.token : undefined
});

const mapDispatchToProps = {
  fetchAuth,
  removeSession
};

module.exports = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(FirstBar));
