import React from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router';
import APPCONFIG from 'constants/Config';
import NavLeftList from './NavLeftList';
import NavRightList from './NavRightList';
import FirstBar from './FirstBar';
import SecondBar from './SecondBar';

class Header extends React.Component {
  componentDidMount() {
    const sidebarToggler = this.sidebarBtn;
    const $sidebarToggler = $(sidebarToggler);
    const $body = $('#body');

    $sidebarToggler.on('click', (e) => {
      // _sidebar.scss, _page-container.scss
      $body.toggleClass('sidebar-mobile-open');
    });
  }

  render() {
    const { isFixedHeader, colorOption, hideFirstBar, hideSecondBar } = this.props;

    return (
    <div>
    {
      hideFirstBar?
      null :
      <FirstBar/>
    }
      {
        hideSecondBar?
        null:
        <SecondBar/>
      }

    </div>

    );
  }
}


const mapStateToProps = state => ({
  colorOption: state.settings.colorOption,
  isFixedHeader: state.settings.isFixedHeader,
  hideFirstBar: state.settings.hideFirstBar,
  hideSecondBar: state.settings.hideSecondBar
});

module.exports = connect(
  mapStateToProps
)(Header);
