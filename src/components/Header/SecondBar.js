import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import FontIcon from 'material-ui/FontIcon';
const grayLight= '#3D5468';
const gray = '#2F4457';

const style = {
  drawer : {backgroundColor: gray },
  menuFirstItem : {backgroundColor: '#AF3789', color: 'white'},
  menuItem : {backgroundColor: grayLight, color: 'white', 'border': `solid ${gray} 0.5px`},
  rightIcon: {color: 'white'}
}


MenuItem.defaultProps.disableTouchRipple = true;

export default class SecondBar extends React.Component{

  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  handleToggle = () => this.setState({open: !this.state.open});

  handleClose = () => this.setState({open: false});

  render(){
    return(
      <nav>
        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={(open) => this.setState({open})}
          containerStyle={style.drawer}
        >
          <MenuItem primaryText="Menú" onTouchTap={this.handleClose} style={style.menuFirstItem} rightIcon={<FontIcon className="material-icons" style={style.rightIcon}>clear</FontIcon>}/>
          <Divider/>
          <MenuItem primaryText="Quiénes Somos" onTouchTap={this.handleClose} style={style.menuItem}/>
          <MenuItem primaryText="Noticias" onTouchTap={this.handleClose} style={style.menuItem}/>
          <MenuItem primaryText="Donar" onTouchTap={this.handleClose} style={style.menuItem}/>
          <MenuItem primaryText="Contacto" onTouchTap={this.handleClose} style={style.menuItem}/>
          <MenuItem primaryText="Categorías" onTouchTap={this.handleClose} style={style.menuItem}/>
        </Drawer>

        <div className="container">
          <div className="row">
            <div className="col-md-3 col-sm-4 col-xs-3">
              <div className="mm-toggle-wrap">
                <div className="mm-toggle" onClick={this.handleToggle}> <i className="fa fa-align-justify"></i> </div>
                <span className="mm-label hidden-xs">Categories</span> </div>
              <div className="mega-container visible-lg visible-md visible-sm">
                <div className="navleft-container">
                  <div className="mega-menu-title">
                    <h3>Categorias</h3>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-9 col-sm-6 col-md-6">
              {/* Search */}

              <div className="top-search hidden-xs hidden-sm" >
                <div id="search">
                  <form>
                    <div className="input-group">
                      <input type="text" className="form-control" placeholder="Search" name="search"/>
                      <select className="cate-dropdown hidden-xs hidden-sm" name="category_id">
                        <option>All Categories</option>
                        <option>women</option>
                        <option>&nbsp;&nbsp;&nbsp;Chair </option>
                        <option>&nbsp;&nbsp;&nbsp;Decoration</option>
                        <option>&nbsp;&nbsp;&nbsp;Lamp</option>
                        <option>&nbsp;&nbsp;&nbsp;Handbags </option>
                        <option>&nbsp;&nbsp;&nbsp;Sofas </option>
                        <option>&nbsp;&nbsp;&nbsp;Essential </option>
                        <option>Men</option>
                        <option>Electronics</option>
                        <option>&nbsp;&nbsp;&nbsp;Mobiles </option>
                        <option>&nbsp;&nbsp;&nbsp;Music &amp; Audio </option>
                      </select>
                      <button className="btn-search hidden-md-up" type="button"><i className="fa fa-filter"></i></button>
                      <button className="btn-search" type="button"><i className="fa fa-search"></i></button>
                    </div>
                  </form>
                </div>
              </div>

              {/* End Search */}
            </div>

          </div>
        </div>
      </nav>
    )
  }
}
