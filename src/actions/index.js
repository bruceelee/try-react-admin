import * as types from '../constants/ActionTypes';
import * as api from '../api';

export function toggleBoxedLayout(isLayoutBoxed) {
  return { type: types.TOGGLE_BOXED_LAYOUT, isLayoutBoxed };
}
export function toggleCollapsedNav(isNavCollapsed) {
  return { type: types.TOGGLE_COLLAPSED_NAV, isNavCollapsed };
}
export function toggleNavBehind(isNavBehind) {
  return { type: types.TOGGLE_NAV_BEHIND, isNavBehind };
}
export function toggleFixedHeader(isFixedHeader) {
  return { type: types.TOGGLE_FIXED_HEADER, isFixedHeader };
}
export function changeSidebarWidth(sidebarWidth) {
  return { type: types.CHANGE_SIDEBAR_WIDTH, sidebarWidth };
}
export function changeColorOption(colorOption) {
  return { type: types.CHANGE_COLOR_OPTION, colorOption };
}
export function changeTheme(themeOption) {
  return { type: types.CHANGE_THEME, theme: themeOption };
}
export function toggleHideFirstBar(isHide) {
  return { type: types.TOGGLE_HIDE_FIRST_BAR, isHide };
}
export function toggleHideSecondBar(isHide) {
  return { type: types.TOGGLE_HIDE_SECOND_BAR, isHide };
}


/*Login*/
const storeToken = (email, token) => ({
  type: types.ADD_TOKEN ,
  email,
  token,
});

export const fetchAuth = (email) =>
  api.fetchAuth(email).then(response =>
    storeToken(email, response)
  );

export function removeSession(){
    return {type: types.ADD_TOKEN, email: null, token: null }
}