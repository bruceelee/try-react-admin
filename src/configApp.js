import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { applyRouterMiddleware, Router, Route, hashHistory, IndexRedirect, Redirect } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import reducers from './reducers';
import { loadState, saveState } from './localStorage';
import throttle from 'lodash/throttle';
import promise from 'redux-promise';
import { createLogger } from 'redux-logger';


const configureStore = () => {
  /*Persisted data in localStorage is load in state*/
  const persistedState = loadState();
    
  /*Load middlewaaers*/
  let middlewares = [ 
      promise, 
      routerMiddleware(hashHistory) 
  ];
  
  
  if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
  }

  return createStore(
    reducers,
    persistedState,
    applyMiddleware(...middlewares)
  );
};


/*Generate store*/
const store = configureStore();


/* Save state changes in localStorage */
store.subscribe(throttle(() => {
  saveState({
      auth: store.getState().auth
  });
}, 1000));



//Persist the history in the state
const history = syncHistoryWithStore(hashHistory, store);


//Define the routes
const rootRoute = {
  childRoutes: [{
    path: '/',
    component: require('./containers/App'),
    indexRoute: { onEnter: (nextState, replace) => replace('/app/dashboard') },
    childRoutes: [
      require('./routes/app'),
      require('./routes/404'),
      require('./routes/500'),
      require('./routes/confirmEmail'),
      require('./routes/forgotPassword'),
      require('./routes/lockScreen'),
      require('./routes/login'),
      require('./routes/signUp'),
      require('./routes/fullscreen'),
      {
        path: '*',
        indexRoute: { onEnter: (nextState, replace) => replace('/404') },
      }
    ]
  }]
};


export {
  store,
  history,
  rootRoute
}