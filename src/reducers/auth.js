import {
  ADD_TOKEN
} from '../constants/ActionTypes';

const auth = (state = null, action) => {
    // console.log(action)
  switch (action.type) {
    case ADD_TOKEN:
      return {
        ...state,
        email: action.email,
        token: action.token
      };
    default:
      return state;
  }
};

module.exports = auth;
