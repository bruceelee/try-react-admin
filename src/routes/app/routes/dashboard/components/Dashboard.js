import React from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import QueueAnim from 'rc-queue-anim';

const imagePath = 'assets/images/background/';

import {
  toggleHideSecondBar
} from '../../../../../actions';

import {
  getCategories
} from '../../../../../api';

const styles = {
  background : {
    backgroundImage: `url('${imagePath}background_home.jpeg')`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
    height: '800px',
    zIndex: 1,
    backgroundPosition: '30% 90%',
    backgroundAttachment: 'fixed',

  }
}

class DashboardContent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
      categorieValue: 0,
      categories: []
    }
  }
  componentWillMount() {
    this.props.handleToggleHideSecondBar(true);

    getCategories().then(categories=>{
      this.setState({
        categories: categories.data.map(c =>{ return { name: c.name, id: c.id } })
      });
    });
  }

  componentWillUnmount(nextProps) {
    this.props.handleToggleHideSecondBar(false);
  }

  handleSearchButton = (event)=>{
    event.preventDefault();
    this.goSearch();
  }

  handleKeyPress = (event) =>{
    if(event.key == 'Enter'){
      event.preventDefault();
      this.goSearch();
    }
  }

  updateSearchValue = (event) => {
    this.setState({
      searchValue: event.target.value
    });
  }

  updateCategorieValue = (event) =>{
    this.setState({
      categorieValue: event.target.value
    });
  }

  goSearch(){
    let searchParams= this.state.searchValue ? `?v=${this.state.searchValue}`: '';
    searchParams += (this.state.searchValue ? '&&' : '?') + `c=${this.state.categorieValue}`;
    this.props.router.push(`/app/search${searchParams}`);
  }



  render() {
    console.log(this.state);
    return (
        <section className="container-fluid no-breadcrumbs chapter" style={styles.background}>

            <div className="top-search" >
                    <div id="search">
                      <form>
                        <div className="input-group">
                          <input type="text" className="form-control" value={this.state.searchValue} onChange={this.updateSearchValue} placeholder="Search" onKeyPress={this.handleKeyPress}/>

                            <select className="cate-dropdown hidden-xs hidden-sm" value={this.state.categorieValue} onChange={this.updateCategorieValue}>
                              <option value={0}>Todos</option>
                              {
                                this.state.categories.length >0  &&
                                this.state.categories.map((c) => {
                                  return <option value={c.id} key={c.id}>{c.name}</option>;
                                })
                              }
                            </select>




                          {/*<button className="btn-search hidden-md-up" type="button"><i className="fa fa-filter"></i></button> */}
                          <button className="btn-search" type="button" onClick={this.handleSearchButton}><i className="fa fa-search"></i></button>
                        </div>
                      </form>
                    </div>
                  </div>

        </section>
    )
 }

}

const mapDispatchToProps = {
    handleToggleHideSecondBar : toggleHideSecondBar
};

module.exports = connect(
    null,
    mapDispatchToProps
)(DashboardContent);
