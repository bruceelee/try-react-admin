import React from 'react';
import Slider from 'react-slick';
import {Link} from 'react-router';

import classnames from 'classnames';
import QueueAnim from 'rc-queue-anim';
import { fecthObject, getProducts } from '../../../../../api';


class SearchContent extends React.Component {
  state = {
      products: []
  }
  componentDidMount(){
      fecthObject('products').then(products=>{
          console.log(products);
          this.setState({
                products
          });
      })

      getProducts().then(products=>{
        console.log(products);
      });
  }

  /*componentDidUpdate(prevProps){
      if(this.props.filter !== prevProps.filter){
          //filter again
      }
  }*/

  navigate(product){
    this.props.router.push(`/app/product/detail/${product.id}`)
  }

  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      adaptiveHeight: true,
      responsive: [ { breakpoint: 768, settings: { slidesToShow: 1,  slidesToScroll: 1 } },
                    { breakpoint: 1024, settings: { slidesToShow: 3 , slidesToScroll: 3 } },
                    { breakpoint: 100000, settings: { slidesToShow: 3 , slidesToScroll: 3 } } ]
    };

    return (
        <section className="container-fluid with-maxwidth no-breadcrumbs chapter" >
          <QueueAnim type="bottom" className="ui-animate">
            <div key="1">
              {
                this.state.products.length === 0 ? <span>No products</span> :
                <Slider {...settings}>
                {
                  this.state.products.map((product, index) => (
                    <a className="item-card" key={index} style={{'padding': '5px'}} onClick={this.navigate.bind(this, product)} >
                      <div className="card__image">
                        <img alt="product" style={{'display': 'inline !important' }} src={product.img} />
                      </div>
                      <div className="card__body card-white" >
                        <div className="card__title">
                          <h6>Accessories</h6>
                          <h4>{product.name}</h4>
                        </div>
                        <div className="card__price">
                          <span className="type--strikethrough">$699.99</span>
                          <span>$649.99</span>
                        </div>
                      </div>
                    </a>
                  ))
                }
              </Slider>
              }
            </div>
          </QueueAnim>
        </section>
    )
 }

}

module.exports = SearchContent;
