import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
};

const TabDescription = () =>(
  <div className="tab-pane" id="description">
    <div className="std">
      <p>Proin lectus ipsum, gravida et mattis vulputate,
        tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in
        faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend
        laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla
        luctus malesuada tincidunt. Nunc facilisis sagittis ullamcorper. Proin
        lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et
        lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et
        ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus
        adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada
        tincidunt. Nunc facilisis sagittis ullamcorper. Proin lectus ipsum,
        gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc.
        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
        cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl
        ut dolor dignissim semper. Nulla luctus malesuada tincidunt.</p>
      <p> Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
      <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi. Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
    </div>
  </div>
)

const TabReviews = () => (
  <div className="tab-pane" id="reviews" >
    <div className="col-sm-5 col-lg-5 col-md-5">
      <div className="reviews-content-left">
        <h2>Customer Reviews</h2>
        <div className="review-ratting">
          <p><a href="#">Amazing</a> Review by Company</p>
          <table>
            <tbody>
              <tr>
                <th>Price</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
              <tr>
                <th>Value</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
              <tr>
                <th>Quality</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
            </tbody>
          </table>
          <p className="author"> Angela Mack<small> (Posted on 16/12/2015)</small> </p>
        </div>
        <div className="review-ratting">
          <p><a href="#">Good!!!!!</a> Review by Company</p>
          <table>
            <tbody>
              <tr>
                <th>Price</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
              <tr>
                <th>Value</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
              <tr>
                <th>Quality</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
            </tbody>
          </table>
          <p className="author"> Lifestyle<small> (Posted on 20/12/2015)</small> </p>
        </div>
        <div className="review-ratting">
          <p><a href="#">Excellent</a> Review by Company</p>
          <table>
            <tbody>
              <tr>
                <th>Price</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
              <tr>
                <th>Value</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
              <tr>
                <th>Quality</th>
                <td><div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div></td>
              </tr>
            </tbody>
          </table>
          <p className="author"> Jone Deo<small> (Posted on 25/12/2015)</small> </p>
        </div>
      </div>
    </div>
    <div className="col-sm-7 col-lg-7 col-md-7">
      <div className="reviews-content-right">
        <h2>Write Your Own Review</h2>
        <form>
          <h3>You're reviewing: <span>Donec Ac Tempus</span></h3>
          <h4>How do you rate this product?<em>*</em></h4>
          <div className="table-responsive reviews-table">
            <table>
              <tbody>
                <tr>
                  <th></th>
                  <th>1 star</th>
                  <th>2 stars</th>
                  <th>3 stars</th>
                  <th>4 stars</th>
                  <th>5 stars</th>
                </tr>
                <tr>
                  <td>Quality</td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                </tr>
                <tr>
                  <td>Price</td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                </tr>
                <tr>
                  <td>Value</td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                  <td><input type="radio"/></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="form-area">
            <div className="form-element">
              <label>Nickname <em>*</em></label>
              <input type="text"/>
            </div>
            <div className="form-element">
              <label>Summary of Your Review <em>*</em></label>
              <input type="text"/>
            </div>
            <div className="form-element">
              <label>Review <em>*</em></label>
              <textarea></textarea>
            </div>
            <div className="buttons-set">
              <button className="button submit" title="Submit Review" type="submit"><span><i className="fa fa-thumbs-up"></i> &nbsp;Review</span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
)

const TabProductTags = () => (
  <div className="tab-pane" id="product_tags">
    <div className="box-collateral box-tags">
      <div className="tags">
        <form id="addTagForm" action="#" method="get">
          <div className="form-add-tags">
            <div className="input-box">
              <label >Add Your Tags:</label>
              <input className="input-text" name="productTagName" id="productTagName" type="text"/>
              <button type="button" title="Add Tags" className="button add-tags"><i className="fa fa-plus"></i> &nbsp;<span>Add Tags</span> </button>
            </div>
            {/*input-box*/}
          </div>
        </form>
      </div>
      {/*tags*/}
      <p className="note">Use spaces to separate tags. Use single quotes (') for phrases.</p>
    </div>
  </div>
)

export default class ProductTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0
    };
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  };

  render(){
    return(
      <div>
        <Tabs
          tabItemContainerStyle={{backgroundColor:"#F4A137"}}
          inkBarStyle={{backgroundColor:"#222222"}}
          onChange={this.handleChange}
          value={this.state.slideIndex}
        >
          <Tab label="Description" value={0} />
          <Tab label="Reviews" value={1} />
          <Tab label="Tags" value={2} />
        </Tabs>
        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
          <TabDescription style={styles.slide}/>
          <TabReviews style={styles.slide}/>
          <TabProductTags style={styles.slide}/>
        </SwipeableViews>
      </div>
    )
  }
}
