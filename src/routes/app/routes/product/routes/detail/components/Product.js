import React from 'react';

export default class Product extends React.Component{
  constructor(props) {
    super(props);
  }

  render(){
    return(
      <div className="large-image">
        <a className="cloud-zoom">
          <img className="zoom-img" src={this.props.product.img} alt="products"/>
        </a>
      </div>
  )}
}
