import React from 'react';
import classnames from 'classnames';
import QueueAnim from 'rc-queue-anim';

import Product from './Product';
import ProductTabs from './ProductTabs';
import ProductDetailArea from './ProductDetailArea';
import GalleryProduct from './GalleryProduct';

import { fetchSingleProduct } from '../../../../../../../api';


export default class DetailProduct extends React.Component {
  state = {
      product: undefined
  }
  componentDidMount(){
      fetchSingleProduct(this.props.params.id).then( product =>{
          this.setState({
                product
          });
      })
  }
    
  render(){
    return (
      <section className="container-fluid with-maxwidth no-breadcrumbs chapter" >
        {
        this.state.product ? 
         <div className="main-container col1-layout">
            <div className="container">
              <div className="row">
                <div className="col-main">
                  <div className="product-view-area">
                    <div className="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                      <div className="icon-sale-label sale-left">Sale</div>
                      <Product product={this.state.product }/>
                    {/*<GalleryProduct products={products}/> */}
                    </div>

                    <ProductDetailArea/>
                  </div>
                </div>

              </div>

              <ProductTabs/>
            </div>
          </div>
          :  <span>Loading...</span>
        }
      </section>
    );
  }
}


module.exports = DetailProduct;
