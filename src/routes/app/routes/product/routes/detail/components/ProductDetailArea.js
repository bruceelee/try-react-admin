import React from 'react';
export default class ProductDetailArea extends React.Component{

  render(){
    return (
      <div className="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
        <div className="product-name">
          <h1>Lorem Ipsum is simply</h1>
        </div>
        <div className="price-box">
          <p className="special-price"> <span className="price-label">Special Price</span> <span className="price"> $329.99 </span> </p>
          <p className="old-price"> <span className="price-label">Regular Price:</span> <span className="price"> $359.99 </span> </p>
        </div>
        <div className="ratings">
          <div className="rating"> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-o"></i> <i className="fa fa-star-o"></i> </div>
          <p className="rating-links"> <a href="#">1 Review(s)</a> <span className="separator">|</span> <a href="#">Add Your Review</a> </p>
          <p className="availability in-stock pull-right">Availability: <span>In Stock</span></p>
        </div>
        <div className="short-description">
          <h2>Quick Overview</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum.</p>
          <p> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
        </div>
        <div className="product-color-size-area">
          <div className="color-area">
            <h2 className="saider-bar-title">Color</h2>
            <div className="color">
              <ul>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
              </ul>
            </div>
          </div>
          <div className="size-area">
            <h2 className="saider-bar-title">Size</h2>
            <div className="size">
              <ul>
                <li><a href="#">S</a></li>
                <li><a href="#">L</a></li>
                <li><a href="#">M</a></li>
                <li><a href="#">XL</a></li>
                <li><a href="#">XXL</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div className="product-variation">
          <form action="#" method="post">
            <div className="cart-plus-minus">
              <label >Quantity:</label>
              <div className="numbers-row">
                <div  className="dec qtybutton"><i className="fa fa-minus">&nbsp;</i></div>
                <input type="text" className="qty" title="Qty" value="1"  id="qty" name="qty"/>
                <div  className="inc qtybutton"><i className="fa fa-plus">&nbsp;</i></div>
              </div>
            </div>
            <button className="button pro-add-to-cart" title="Add to Cart" type="button"><span><i className="fa fa-shopping-cart"></i> Add to Cart</span></button>
          </form>
        </div>
        <div className="product-cart-option">
          <ul>
            <li><a href="#"><i className="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
            <li><a href="#"><i className="fa fa-retweet"></i><span>Add to Compare</span></a></li>
            <li><a href="#"><i className="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
          </ul>
        </div>
      </div>
    )}
}
