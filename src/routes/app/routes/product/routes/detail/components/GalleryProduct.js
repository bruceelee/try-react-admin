import React from 'react';
import Slider from 'react-slick';

export default class GalleryProduct extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      products: this.props.products
    };

  }

  render() {
    var settings = {
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      adaptiveHeight: true,
      responsive: [ { breakpoint: 768, settings: { slidesToShow: 1,  slidesToScroll: 1 } },
                    { breakpoint: 1024, settings: { slidesToShow: 3 , slidesToScroll: 3 } },
                    { breakpoint: 100000, settings: { slidesToShow: 3 , slidesToScroll: 3 } } ]
    }

    return (
        <div className="col-md-5">
        	<Slider {...settings}>
            {
            this.state.products.map((product, index) => (
              <a className="item-card" key={index} style={{'padding': '5px'}}>
                <div className="card__image" style={{height: '120px'}}>
                  <img alt="product" style={{'display': 'inline !important' }} src={product.img} />
                </div>
              </a>
              ))
            }
          </Slider>
        </div>
    );
  }
}
