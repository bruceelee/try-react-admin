module.exports = {
  path: 'app',
  getChildRoutes(partialNextState, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./routes/dashboard'),
        require('./routes/ecommerce'),
        require('./routes/pageLayouts'),
        require('./routes/pages'),
        require('./routes/product'),
        require('./routes/search'),
      ]);
    });
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/MainApp'));
    });
  }
};
