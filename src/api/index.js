import axios from 'axios';

const serverUrl = 'http://192.168.10.111/api/v1/'
const serverAuthUrl = 'http://192.168.10.111/'

const productImagePath = 'assets/images-demo/products/';
const fakeDatabase = {
    products: [
          {
            id: 1,
            name: 'Silver Watch',
            img: `${productImagePath}watch-silver.png`,
            sash: 'sash-primary',
            sash_icon: 'star',
            sash_text: 'Featured'
          }, {
            id: 2,
            name: 'Black Watch',
            img: `${productImagePath}watch-black.png`,
            sash: 'sash-info',
            sash_icon: 'card_giftcard',
            sash_text: 'Gift'
          }, {
            id: 3,
            name: 'Red Watch',
            img: `${productImagePath}watch-edition-red.png`,
            sash: 'sash-danger',
            sash_icon: 'flash_on',
            sash_text: 'Popular'
          }, {
            id: 4,
            name: 'Blue Watch',
            img: `${productImagePath}watch-edition-blue.png`,
            sash: '',
            sash_icon: 'info',
            sash_text: 'Featured'
          }, {
            id: 4,
            name: 'Black Watch',
            img: `${productImagePath}watch-edition-black.png`,
            sash: 'sash-danger',
            sash_icon: 'favorite',
            sash_text: 'Best Choice'
          }, {
            id: 5,
            name: 'Sport Watch',
            img: `${productImagePath}watch-sport-blue.png`,
            sash: 'sash-success',
            sash_icon: 'new_releases',
            sash_text: 'New'
          }, {
            id: 6,
            name: 'Sport Watch',
            img: `${productImagePath}watch-sport-green.png`,
            sash: 'sash-warning',
            sash_icon: 'money_off',
            sash_text: 'Free Shipping'
          }, {
            id: 7,
            name: 'Sport Watch',
            img: `${productImagePath}watch-sport-white.png`,
            sash: 'sash-white',
            sash_icon: 'star',
            sash_text: 'Featured'
          }
        ]
};


/*FAKE DATABASE*/

const delay = (ms) =>
  new Promise(resolve => setTimeout(resolve, ms));

export const fecthObject = ( id ) =>
  delay(500).then(() => {
    switch (id) {
      case 'products':
        return fakeDatabase.products;
      default:
        throw new Error(`Unknown filter: ${filter}`);
    }
  });


export const fetchSingleProduct = ( id ) =>
  delay(500).then(() => {
    return fakeDatabase.products.filter(t => t.id == id )[0];
  });


export const fetchAuth = ( email ) =>
    delay(500).then(() => {
    return '123456789';
});


/* Real Calls API*/
export function getCategories(){
  return axios.get(`${serverUrl}categories`);
}

export function getProducts(username){
  return axios.get(`${serverUrl}products`);
}

export function authServer(){
  return axios.get(`${serverAuthUrl}auth/facebook`);
}
